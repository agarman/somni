(ns somni.middleware.headers)

(defn wrap-header
  "Wraps a static header around a request.  Useful for things
  like cache-control being set upon a get."
  [handler header value]
  (fn [request]
    (assoc-in (handler request) [:headers header] value)))
