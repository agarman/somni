;;; Copyright (c) Care Logistics, inc. All rights reserved.
;;; The use and distribution terms for this software are covered by the
;;; Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
;;; which can be found in the file epl-v10.html at the root of this
;;; distribution.
;;; By using this software in any fashion, you are agreeing to be bound by
;;; the terms of this license.
;;; You must not remove this notice, or any other, from this software.

(ns somni.middleware.validation
  (:require [clojure.instant :refer [read-instant-date]]
            [schema
             [coerce :as c]
             [utils :as u]]
            [somni.http.errors :refer [malformed-request]]
            [somni.misc :refer [safe-name]])
  (:import (schema.utils ValidationError)))

(def ^:private error-symbols
  "Translates Prismatic error symbols."
  {'sequential?          "Not a vector"
   'integer?             "Not an integer"
   'keyword?             "Not a keyword"
   'missing-required-key "Missing required key"
   'instance?            "Incorrect value type"})

(defn- explain-better
  [error]
  (let [error* (-> error
                   u/validation-error-explain
                   second
                   first)]
    (if (symbol? error*)
      (error-symbols error*)
      error*)))

(defn- explain-error
  "Presents a user-friendly error."
  [error]
  (cond
    (instance? ValidationError error) (explain-better error)
    (symbol? error) (error-symbols error)
    :else "Cannot explain"))

(defn flatten-errors
  [data]
  (letfn [(aux [data acc trail]
            (cond
              (nil? data)    ()
              (vector? data) (mapcat #(aux %1 acc (conj trail %2)) data (range))
              (map? data)    (mapcat #(aux %1 acc (conj trail %2)) (vals data) (keys data))
              :else          (conj acc [trail data])))]
    (aux data [] [])))

(defn to-errors-map
  [errors]
  (map
    (fn [[p e]] (zipmap [:path :error] [p (explain-error e)]))
    (flatten-errors errors)))

(defn wrap-request-validation
  "
  Returns 400 if the body of a post, put or patch does not match the
  schema.
  "
  [handler schema coercer]

  {:pre [handler schema coercer]}

  (fn [{:as request :keys [body]}]

    (let [checker (c/coercer schema coercer)
          result (checker body)]

      (if-let [errors (some-> result :error to-errors-map)]
        (assoc (malformed-request request) :body {:errors errors})
        (handler (assoc request :body result))))))

