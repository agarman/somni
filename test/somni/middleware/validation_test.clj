(ns somni.middleware.validation-test
  (:require [somni.middleware.validation :refer :all]
            [clojure.test :refer :all]
            [schema.core :as s]))

(def ^:private +schema+ {:user   s/Str
                         :action s/Keyword})

(def ^:private +coercer+ (constantly nil))

(def ^:private h (wrap-request-validation identity +schema+ +coercer+))

(def ^:private req1 {:request-method :put
                     :body           {:user   "bob",
                                      :action :rock-on}})

(def ^:private invalid-req {:request-method :post
                            :body           {:user   "bob",
                                             :action 1234}})

(deftest wrap-schema-validation-test
  (is (= (h req1) req1)
      "Validation lets good data through")

  (is (get-in (h invalid-req) [:body :errors])
      "Validation stops bad data"))


(deftest test-flatten-errors

  (testing "vector"
    (let [errors ['e1 nil 'e2]]
      (is (= [[[0] 'e1] [[2] 'e2]]
             (flatten-errors errors)))))

  (testing "map"
    (let [errors {:x 'e1, :y {:z 'e2}}]
      (is (= [[[:x] 'e1] [[:y :z] 'e2]]
             (flatten-errors errors)))))

  (testing "mixed"
    (let [errors [nil {:x {:y [nil 'e1 nil]}} {:z 'e2}]]
      (is (= [[[1 :x :y 1] 'e1] [[2 :z] 'e2]]
             (flatten-errors errors))))))
